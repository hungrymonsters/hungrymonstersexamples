var game = new Phaser.Game(480, 320, Phaser.CANVAS, 'game');

var PhaserGame = function (game) {
	this.map = null;
	this.layer = null;
	this.player = null;
	this.ready = false;
	this.cursors = false;
};

PhaserGame.prototype = {

	init: function () {

		this.physics.startSystem(Phaser.Physics.ARCADE);

	},

	preload: function () {

		this.load.baseURL = 'http://localhost:4242/';
		this.load.crossOrigin = 'anonymous';

		this.load.tilemap('map', 'assets/test2.json', null, Phaser.Tilemap.TILED_JSON);
		this.load.image('tiles', 'assets/tiles.png');
		this.load.image('cat', 'assets/cat.png');

	},

	create: function () {
		//start create world
		this.map = this.add.tilemap('map');
		this.map.addTilesetImage('tiles', 'tiles');

		this.layer = this.map.createLayer('Tile Layer 1');

		this.map.setCollision(6, true, this.layer);

		this.createPlayer();
	},

	createPlayer: function() {
		this.player = new Player(1, this);
		this.cursors = this.input.keyboard.createCursorKeys();
		this.world.setBounds(0, 0, 960, 640);
		this.camera.follow(this.player.player);
		this.ready = true;
	},

	update: function () {
		if (!this.ready) return;
		
	
		this.player.input.left = this.cursors.left.isDown;
		this.player.input.right = this.cursors.right.isDown;
		this.player.input.up = this.cursors.up.isDown;
		this.player.input.down = this.cursors.down.isDown;
		// this.player.input.fire = game.input.activePointer.isDown;
	
		this.player.update();

	},

	render: function () {

		//                this.game.debug.cameraInfo(this.game.camera, 32, 32);
	}

};

game.state.add('Game', PhaserGame, true);