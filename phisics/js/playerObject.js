Player = function (index, game) {
    console.log("create player id" + index)
    this.cursor = {
        left: false,
        right: false,
        up: false,
        down: false,
        fire: false
    }

    this.input = {
        left: false,
        right: false,
        up: false,
        down: false,
        fire: false
    }

    this.x = 0;
    this.y = 0;

    this.id = index;
    this.game = game;
    this.health = 30;
    this.currentSpeed = 0;

    this.player = this.game.add.sprite(60, 60, 'cat');
    this.player.anchor.set(0.5);

    game.physics.enable(this.player, Phaser.Physics.ARCADE);

    // this.player.body.immovable = false;
    // this.player.body.collideWorldBounds = true;
    // this.player.body.bounce.setTo(0, 0);

    this.player.angle = 90;

    

};

Player.prototype.update = function () {
    this.game.physics.arcade.collide(this.player, this.game.layer);
    
    //fizyka "sterowania" graczem
    // console.log(this.cursor);
    if (this.input.left)
    {
        this.player.angle -= 5;
    }
    else if (this.input.right)
    {
        this.player.angle += 5;
    }	
    if (this.input.up)
    {
        //  The speed we'll travel at
        this.currentSpeed = 90;
    }
    else
    {
        if (this.currentSpeed > 0)
        {
            this.currentSpeed -= 5;
        }
    }

    if (this.currentSpeed > 0)
    {
        this.game.physics.arcade.velocityFromRotation(this.player.rotation, this.currentSpeed, this.player.body.velocity);
    }	
	else
	{
		this.game.physics.arcade.velocityFromRotation(this.player.rotation, 0, this.player.body.velocity);
	}

};
