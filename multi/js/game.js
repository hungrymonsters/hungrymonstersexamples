var game = new Phaser.Game(480, 320, Phaser.CANVAS, 'game');

var PhaserGame = function (game) {
	this.map = null;
	this.layer = null;
	this.player = null;
	this.ready = false;
	this.myId = null;
	this.playerList = {};
	
	this.eurecaClient = null;
};

PhaserGame.prototype = {

	init: function () {

		this.physics.startSystem(Phaser.Physics.ARCADE);

	},

	preload: function () {

		this.load.baseURL = 'http://localhost:8000/';
		this.load.crossOrigin = 'anonymous';

		this.load.tilemap('map', 'assets/test2.json', null, Phaser.Tilemap.TILED_JSON);
		this.load.image('tiles', 'assets/tiles.png');
		this.load.image('cat', 'assets/cat.png');

	},

	create: function () {
		//start create world
		this.map = this.add.tilemap('map');
		this.map.addTilesetImage('tiles', 'tiles');

		this.layer = this.map.createLayer('Tile Layer 1');

		this.map.setCollision(6, true, this.layer);

		//start create client 
		this.eurecaClient = new Eureca.Client();
		
		this.eurecaClient.ready(function (proxy) {		
			eurecaServer = proxy;
		});

		var that = this;
		this.eurecaClient.exports.setId = function(id) 
		{
			//create() is moved here to make sure nothing is created before uniq id assignation
			that.myId = id;
			that.createPlayer();
			eurecaServer.handshake();
			that.ready = true;
		}

		this.eurecaClient.exports.spawnEnemy = function(i, x, y)
		{
			if (i == that.myId) return; //this is me
			
			console.log('SPAWN');
			var p = new Player(i, that, true);
			that.playerList[i] = p;
		}

		this.eurecaClient.exports.updateState = function(id, state)
		{
			if (that.playerList[id])  {
				that.playerList[id].cursor = state;
				that.playerList[id].player.x = state.x;
				that.playerList[id].player.y = state.y;
				that.playerList[id].player.angle = state.angle;
				// that.playerList[id].turret.rotation = state.rot;
				that.playerList[id].update();
			}
		}

	},

	createPlayer: function() {
		this.player = new Player(this.myId, this);
		this.playerList[this.myId] = this.player; 
		this.cursors = this.input.keyboard.createCursorKeys();
		this.world.setBounds(0, 0, 960, 640);
		this.camera.follow(this.player.player);
	},

	update: function () {
		if (!this.ready) return;
	
		this.player.input.left = this.cursors.left.isDown;
		this.player.input.right = this.cursors.right.isDown;
		this.player.input.up = this.cursors.up.isDown;
		this.player.input.down = this.cursors.down.isDown;
		// this.player.input.fire = game.input.activePointer.isDown;

		// this.player.update();
		for(var id in this.playerList) {

			this.playerList[id].update();
		}


	},

	render: function () {

		//                this.game.debug.cameraInfo(this.game.camera, 32, 32);
	}

};

game.state.add('Game', PhaserGame, true);