var game = new Phaser.Game(720, 480, Phaser.CANVAS, 'game');

var PhaserGame = function (game) {
	this.map = null;
	this.layer = null;
	this.player = null;
	this.ready = false;
	this.cursors = false;
};

PhaserGame.prototype = {

	init: function () {

		this.physics.startSystem(Phaser.Physics.ARCADE);

	},

	preload: function () {

		this.load.baseURL = 'http://localhost:4242/';
		this.load.crossOrigin = 'anonymous';

		this.load.tilemap('map', 'assets/map.json', null, Phaser.Tilemap.TILED_JSON);
		this.load.image('deser', 'assets/deser32x32.png');
		this.load.image('cat', 'assets/cat.png');

	},

	create: function () {
		//start create world
		this.map = this.add.tilemap('map');
		this.map.addTilesetImage('deser', 'deser');

		this.layer = this.map.createLayer('Tile Layer 1');

		/* WALLs */
		this.map.setCollision(1, true, this.layer);
		this.map.setCollision(2, true, this.layer);
		this.map.setCollision(3, true, this.layer);
		this.map.setCollision(9, true, this.layer);
		this.map.setCollision(10, true, this.layer);
		this.map.setCollision(11, true, this.layer);
		this.map.setCollision(17, true, this.layer);
		this.map.setCollision(18, true, this.layer);
		this.map.setCollision(19, true, this.layer);

		//test callback overide wall collision
		this.map.setTileIndexCallback(10, this.wallCollisionHandler, this);

		//test add speed collision
		this.map.setTileIndexCallback(34, this.speedCollisionHandler, this);

		//test cactus collision
		this.map.setTileIndexCallback(31, this.cactusCollisionHandler, this);
		
		this.createPlayer();	

		
	},

	createPlayer: function () {
		this.player = new Player(1, this);
		this.cursors = this.input.keyboard.createCursorKeys();
		this.world.setBounds(0, 0, 3840, 3840);
		this.camera.follow(this.player.player);
		this.ready = true;
	},

	update: function () {
		if (!this.ready) return;

	
		this.player.input.left = this.cursors.left.isDown;
		this.player.input.right = this.cursors.right.isDown;
		this.player.input.up = this.cursors.up.isDown;
		this.player.input.down = this.cursors.down.isDown;
		// this.player.input.fire = game.input.activePointer.isDown;

		this.player.update();
		this.game.physics.arcade.overlap(this.player, this.map, this.collisionHandler, null, this);

	},

	wallCollisionHandler: function (sprite, tile) {
		// console.log('collision detect');
		// tile.alpha = 0.2;

		this.player.wallCollision();
		this.layer.dirty = true;

		return true;

	},

	speedCollisionHandler: function (sprite, tile) {
		// console.log('collision detect');
		// tile.alpha = 0.2;

		this.player.speedCollision();
		// this.layer.dirty = true;

		return true;

	},

	cactusCollisionHandler: function (sprite, tile) {		
		this.player.cactusCollision();

		return true;

	},

	render: function () {

		//                this.game.debug.cameraInfo(this.game.camera, 32, 32);
	}

};

game.state.add('Game', PhaserGame, true);